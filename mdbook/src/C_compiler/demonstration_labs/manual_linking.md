# Performance Lab - manual_linking

## Manual Linking

1. Declare and define a simple function as a header
2. Write a source file that calls the header's function
3. Manually link the header's definition and your source file
